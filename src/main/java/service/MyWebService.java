package service;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService(serviceName = "NewWebService")
public class MyWebService {

@WebMethod(operationName = "isServiceUp")
public String isServiceUp(){
    return "Yes.It is UP!";
}

}